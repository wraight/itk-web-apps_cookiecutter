# itk-web-apps_cookieCutter

[cookie cutter](https://cookiecutter.readthedocs.io/en/1.7.2/) template for itk-web-apps

---

## Branch Options

To create from template:

`> cookiecutter -c BRANCH_NAME https://gitlab.cern.ch/wraight/itk-web-apps_cookiecutter`

options:
| BRANCH_NAME | description |
|---|---|
| newApp |  create new app |
| newPage | create new page |

### App directory
  * _directory_name_
    - userPages
      - \_\_init\_\_.py
      - CommonCode.py
      - pageC\__page_name_
    - README.md
    - requirements.txt
